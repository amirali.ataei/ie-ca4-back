<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
  <title>JSP - Hello World</title>
</head>
<body>
<h1><%= request.getAttribute("test") %></h1>
<br/>
<form action="" method="POST" >
  <input id="form_action" type="hidden" name="action" value="add">
  <input id="form_code" type="hidden" name="course_code" value="8101001">
  <input id="form_class_code" type="hidden" name="class_code" value="01">
  <button type="submit">Add</button>
</form>

<form action="" method="POST">
  <button type="submit" name="action" value="submit">Submit Plan</button>
  <button type="submit" name="action" value="reset">Reset</button>
</form>

</body>
</html>