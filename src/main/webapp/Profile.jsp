<%@ page import="com.example.CA3.java.Student" %>
<%@ page import="com.example.CA3.java.Grade" %>
<%@ page import="static com.example.CA3.java.Tools.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.example.CA3.java.Course" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile</title>
    <style>
        li {
        	padding: 5px
        }
        table{
            width: 10%;
            text-align: center;
        }
    </style>
</head>

<%
    Student student = (Student) request.getAttribute("student");
    ArrayList<Grade> passedCourses = (ArrayList<Grade>) request.getAttribute("passedCourses");
%>

<body>
    <a href="/">Home</a>
    <ul>
        <li id="std_id">Student Id: <%=student.getStudentId()%></li>
        <li id="first_name">First Name: <%=student.getName()%></li>
        <li id="last_name">Last Name: <%=student.getSecondName()%></li>
        <li id="birthdate">Birthdate: <%=student.getBirthDate()%></li>
        <li id="gpa">GPA: <%=student.getGPA()%></li>
        <li id="tpu">Total Passed Units: <%=student.getNumOfPassedUnits()%></li>
    </ul>
    <table>
        <tr>
            <th>Code</th>
            <th>Grade</th>
        </tr>
        <%for(Grade course : passedCourses){%>
        <tr>
            <td><%=course.getCode()%></td>
            <td><%=course.getGrade()%></td>
        </tr>
        <%}%>
    </table>
</body>
</html>