package com.example.CA3.exceptions;

public class HadPassedCourseException extends Exception{
    private String name;
    public HadPassedCourseException(String _name) {
        name = _name;
    }
    @Override
    public String toString() {
        return name + " had been passed.";
    }
}
