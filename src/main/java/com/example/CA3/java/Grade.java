package com.example.CA3.java;

import static com.example.CA3.java.Tools.getCourse;

public class Grade {
    private String code;
    private double grade;
    private int term;

    public Grade(String _code, double _grade, int _term) {
        code = _code;
        grade = _grade;
        term = _term;
    }

    public String getCode() {
        return code;
    }

    public double getGrade() {
        return grade;
    }

    public int getTerm() {
        return term;
    }

    public int getUnits() {
        int units = 0;
        try {
            units = getCourse(code, "01").getUnits();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return units;
    }

    public String getName() {
        String name = "";
        try {
            name = getCourse(code, "01").getName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }
}
