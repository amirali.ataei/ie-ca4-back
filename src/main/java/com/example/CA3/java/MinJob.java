package com.example.CA3.java;

import static com.example.CA3.java.Tools.courses;

public class MinJob implements Runnable {
    @Override
    public void run(){
        for(Course course : courses) {
            while(course.getWaitingStudentsSize() > 0 && course.getSignedUp() < course.getCapacity()){
                course.popWaiting();
            }
        }
    }
}
