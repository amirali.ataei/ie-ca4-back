package com.example.CA3.servlet;

import com.example.CA3.java.Course;
import com.example.CA3.java.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

import static com.example.CA3.java.Tools.*;

@WebServlet(name = "CoursesServlet", value = "/courses")
public class CoursesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(studentId.equals(""))
            request.getRequestDispatcher("/login").forward(request, response);

        String action = "";
        if(request.getParameter("action") != null)
            action = (String) request.getParameter("action");

        ArrayList<Course> filterCourses = courses;

        Student student = null;
        try {
            student = getStudent(studentId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert student != null;

        filterCourses = filter();

        switch (action){
            case "wait":
                String code = (String) request.getParameter("course_code");
                String classCode = (String) request.getParameter("class_code");
                Course course = null;
                try {
                    course = getCourse(code, classCode);
                    course.setWaiting(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    course.setWaiting(true);
                    student.addToWeeklySchedule(course);
                    selectedCourses.add(course);
                } catch (Exception e) {
                    log(e.toString());
                }
                break;
            case "add":
                code = (String) request.getParameter("course_code");
                classCode = (String) request.getParameter("class_code");
                course = null;
                try {
                    course = getCourse(code, classCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    student.addToWeeklySchedule(course);
                    selectedCourses.add(course);
                } catch (Exception e) {
                    log(e.toString());
                }
                break;
            case "remove":
                code = (String) request.getParameter("course_code");
                classCode = (String) request.getParameter("class_code");

                course = null;
                try {
                    course = getCourse(code, classCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    student.removeFromWeeklySchedule(course);
                    selectedCourses.remove(course);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "submit":
                try {
                    student.setWeeklySchedule(selectedCourses);
                    student.finalizeWeeklySchedule();
                    lastSubmit = new ArrayList<>(selectedCourses);
                } catch (Exception e) {
                    request.setAttribute("Exception", e.toString());
                    request.getRequestDispatcher("/SubmitFailed.jsp").forward(request, response);
                    log(e.toString());
                }
                break;
            case "reset":
                student.setWeeklySchedule(lastSubmit);
                selectedCourses = new ArrayList<>(lastSubmit);
                break;
            case "search":
                searchFilter = (String) request.getParameter("search");
                filterCourses = filter();
                break;
            case "clear":
                searchFilter = "";
                filterCourses = courses;
                break;
        }

        int units = student.getWeeklySchedule().getTotalUnits();

        request.setAttribute("studentId", studentId);
        request.setAttribute("searchFilter", searchFilter);
        request.setAttribute("selectedCourses", selectedCourses);
        request.setAttribute("totalUnits", units);
        request.setAttribute("filterCourses", filterCourses);

        request.getRequestDispatcher("/Courses.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
