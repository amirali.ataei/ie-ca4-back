package com.example.CA3.servlet;

import com.example.CA3.java.Course;
import com.example.CA3.java.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

import static com.example.CA3.java.Tools.*;

@WebServlet(name = "PlanServlet", value = "/plan")
public class PlanServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if(studentId.equals(""))
            request.getRequestDispatcher("/login").forward(request, response);

        Student student = null;
        try {
            student = getStudent(studentId);
        } catch (Exception e) {
            e.printStackTrace();
        }


        String[] times = new String[5];
        times[0] = "7:30-9:00";
        times[1] = "9:00-10:30";
        times[2] = "10:30-12:00";
        times[3] = "14:00-15:30";
        times[4] = "16:00-17:30";

        String[] sat = new String[5];
        String[] sun = new String[5];
        String[] mon = new String[5];
        String[] tue = new String[5];
        String[] wed = new String[5];
        for(int i = 0; i < 5; i++) {
            sat[i] = "";
            sun[i] = "";
            mon[i] = "";
            tue[i] = "";
            wed[i] = "";
        }

        for (Course course : lastSubmit){
            String name = course.getName();
            String time =  (String) course.getClassTime().get("time");
            ArrayList<String> days = (ArrayList<String>) course.getClassTime().get("days");
            for(int i = 0; i < 5; i++) {
                if (time.equals(times[i])) {
                    if (days.get(0).equals("Saturday") || days.get(1).equals("Saturday"))
                        sat[i] = name;
                    if (days.get(0).equals("Sunday") || days.get(1).equals("Sunday"))
                        sun[i] = name;
                    if (days.get(0).equals("Monday") || days.get(1).equals("Monday"))
                        mon[i] = name;
                    if (days.get(0).equals("Tuesday") || days.get(1).equals("Tuesday"))
                        tue[i] = name;
                    if (days.get(0).equals("Wednesday") || days.get(1).equals("Wednesday"))
                        wed[i] = name;
                }
            }
        }

        request.setAttribute("times", times);
        request.setAttribute("saturday", sat);
        request.setAttribute("sunday", sun);
        request.setAttribute("monday", mon);
        request.setAttribute("tuesday", tue);
        request.setAttribute("wednesday", wed);
        request.setAttribute("studentId", studentId);
        request.setAttribute("lastSubmit", lastSubmit);

        request.getRequestDispatcher("/Plan.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
